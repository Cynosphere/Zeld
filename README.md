# Zeld
Stripped down version of LegendGear 2 with the only feature people use. Made for those with photosensitivity issues in mind.

## Credits
* Me - coding
* Kat (BlueberryKat) - assets
* NMcCoy - LegendGear 2

## Port?
I'll consider a Fabric port most likely.