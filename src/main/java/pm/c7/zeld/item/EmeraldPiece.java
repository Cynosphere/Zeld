package pm.c7.zeld.item;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.event.entity.player.EntityItemPickupEvent;

import java.util.List;

public class EmeraldPiece extends Item {
    private IIcon chipIcon;
    private IIcon chunkIcon;

    public EmeraldPiece() {
        this.setUnlocalizedName("emerald_piece");
        this.setCreativeTab(CreativeTabs.tabMisc);
        this.setHasSubtypes(true);
    }

    public void addRecipes() {
        ItemStack oneChunk = new ItemStack(this, 1, 0);
        ItemStack eightChunks = new ItemStack(this, 8, 0);
        ItemStack eightChips = new ItemStack(this, 8, 1);
        ItemStack oneChip = new ItemStack(this, 1, 1);
        ItemStack oneEmerald = new ItemStack(Items.emerald);

        GameRegistry.addShapelessRecipe(eightChips, oneChunk);
        GameRegistry.addShapelessRecipe(eightChunks, oneEmerald);

        Object[] chips = new Object[8];
        Object[] chunks = new Object[8];

        for (int i = 0; i < 8; ++i) {
            chips[i] = oneChip;
            chunks[i] = oneChunk;
        }

        GameRegistry.addShapelessRecipe(oneChunk, chips);
        GameRegistry.addShapelessRecipe(oneEmerald, chunks);
    }

    @SubscribeEvent
    public void pickupSound(EntityItemPickupEvent event) {
        ItemStack stack = event.item.getEntityItem();
        Item item = stack.getItem();

        if (item != this || item != Items.emerald)
            return;

        boolean passed = event.entityPlayer.inventory.addItemStackToInventory(stack);
        if (passed) {
            if (item == this) {
                if (stack.getMetadata() == 0 && !event.entityPlayer.worldObj.isRemote) {
                    event.entityPlayer.worldObj.playSoundAtEntity(event.entityPlayer, "zeld:chip_get", 0.3F, 1.0F);
                } else if (stack.getMetadata() == 1 && !event.entityPlayer.worldObj.isRemote) {
                    event.entityPlayer.worldObj.playSoundAtEntity(event.entityPlayer, "zeld:chunk_get", 0.3F, 1.0F);
                }
            } else if (item == Items.emerald && !event.entityPlayer.worldObj.isRemote) {
                event.entityPlayer.worldObj.playSoundAtEntity(event.entityPlayer, "zeld:emerald_get", 0.3F, 1.0F);
            }
        }
    }

    @Override
    public String getUnlocalizedName(ItemStack stack) {
        if (stack.getMetadata() == 1)
            return "item.zeld.emerald_chip";
        return "item.zeld.emerald_chunk";
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void registerIcons(IIconRegister registry) {
        this.chipIcon = registry.registerIcon("zeld:emerald_chip");
        this.chunkIcon = registry.registerIcon("zeld:emerald_chunk");
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void getSubItems(Item item, CreativeTabs tabs, List list) {
        list.add(new ItemStack(item, 1, 0));
        list.add(new ItemStack(item, 1, 1));
    }

    @Override
    @SideOnly(Side.CLIENT)
    public IIcon getIconFromDamage(int damage) {
        if (damage == 1)
            return this.chunkIcon;
        return this.chipIcon;
    }

    @Override
    public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player) {
        if (stack.stackSize < 8) {
            return stack;
        }

        stack.stackSize -= 8;

        ItemStack merged;
        if (stack.getMetadata() == 0) {
            merged = new ItemStack(this, 1, 1);
            if (!world.isRemote) {
                world.playSoundAtEntity(player, "zeld:chunk_get", 0.3F, 1.0F);
            }
        } else {
            merged = new ItemStack(Items.emerald);
            if (!world.isRemote) {
                world.playSoundAtEntity(player, "zeld:emerald_get", 0.3F, 1.0F);
            }
        }

        if (!player.inventory.addItemStackToInventory(stack)) {
            ForgeHooks.onPlayerTossEvent(player, merged, false);
        }

        return stack;
    }
}
