package pm.c7.zeld;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import pm.c7.zeld.item.EmeraldPiece;

@Mod(modid = Zeld.MODID, version = Zeld.VERSION)
public class Zeld {
    public static final String MODID = "zeld";
    public static final String VERSION = "@MOD_VERSION@";

    public static final Zeld INSTANCE = new Zeld();

    public static final EmeraldPiece EMERALD_PIECE = new EmeraldPiece();

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        GameRegistry.registerItem(EMERALD_PIECE, "emerald_piece");
        FMLCommonHandler.instance().bus().register(EMERALD_PIECE);
        EMERALD_PIECE.addRecipes();
    }
}